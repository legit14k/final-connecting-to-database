﻿/// <summary>
/// Jason Grimberg
/// Final
/// Cars class
/// </summary>
namespace JasonGrimberg_Final
{
    public class Cars
    {
        // Variables to hold data
        string make;
        string model;
        decimal year;
        decimal mpgCity;
        decimal mpgHighway;
        int imageIndex;
        string carID;

        // Constructors
        public string Make { get => make; set => make = value; }
        public string Model { get => model; set => model = value; }
        public decimal Year { get => year; set => year = value; }
        public decimal MpgCity { get => mpgCity; set => mpgCity = value; }
        public decimal MpgHighway { get => mpgHighway; set => mpgHighway = value; }
        public int ImageIndex { get => imageIndex; set => imageIndex = value; }
        public string CarID { get => carID; set => carID = value; }

        // Override string
        public override string ToString()
        {
            return $"ID: {CarID}\r\n Make: {Make}\r\n Model: {Model}\r\n Year: {Year}\r\n City: {MpgCity}\r\n Highway: {MpgHighway}";
        }
    }
}
