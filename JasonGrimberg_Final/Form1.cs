﻿using System;
using System.Data;
using System.IO;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
/// <summary>
/// Jason Grimberg
/// Coding exercise: Final connecting to database
/// </summary>
namespace JasonGrimberg_Final
{
    public partial class mainForm : Form
    {
        // -----------------------------------------------------------------------------
        // EventHandler call
        // -----------------------------------------------------------------------------
        public EventHandler ObjectAdded;
        public EventHandler SelectionChanged;
        public EventHandler<ModifyObjectEventArgs> ModifyObject;

        // Connection string variable
        MySqlConnection conn = new MySqlConnection();

        // Instance variable for the data
        DataTable theData = new DataTable();

        // Create the SQL statement to download the data
        string dwnSQL = "SELECT vehicleId, make, model, year, mpgHighway, mpgCity " +
            "FROM vehicle " +
            "ORDER BY engID " +
            "LIMIT 25";
        
        // Instance variable for the current row
        int row = 0;

        // -----------------------------------------------------------------------------
        // Classes
        // -----------------------------------------------------------------------------
        public class ModifyObjectEventArgs : EventArgs
        {
            // Implement the ListViewItem to the modified object
            ListViewItem ObjectToModify;

            // Getters and setters for the ListViewItem
            public ListViewItem ObjectToModify1
            {
                // Get the object to modify
                get
                {
                    return ObjectToModify;
                }

                // Set the object to modify (selected item in list-view)
                set
                {
                    ObjectToModify = value;
                }
            }

            // Event argument for the modified object
            public ModifyObjectEventArgs(ListViewItem lvi)
            {
                ObjectToModify = lvi;
            }
        }

        // -----------------------------------------------------------------------------
        // Initialization of the component
        // -----------------------------------------------------------------------------
        public mainForm()
        {
            InitializeComponent();
        }

        // -----------------------------------------------------------------------------
        // Car data components
        // -----------------------------------------------------------------------------
        // Selected car
        public Cars SelectedObject
        {
            // Get the selected object
            get
            {
                if (carListView.SelectedItems.Count > 0)
                {
                    // Set variable to the current selected item
                    var currentIndex = carListView.SelectedItems[0].Index + 1;
                    // Change the label to the current index
                    lblCurrentRecord.Text = currentIndex.ToString();
                    // Return the current selection to a new tag
                    return carListView.SelectedItems[0].Tag as Cars;
                }
                else
                {
                    // Return new instance of cars
                    return new Cars();
                }

            }
        }

        // New Car data
        public Cars Data
        {
            // Get all car data
            get
            {
                Cars c = new Cars();
                c.CarID = tbID.Text;
                c.Make = tbMake.Text;
                c.Model = tbModel.Text;
                c.Year = numYear.Value;
                c.MpgCity = numCity.Value;
                c.MpgHighway = numHighway.Value;
                c.ImageIndex = 0;
                return c;
            }
            // Set all car data
            set
            {
                tbID.Text = value.CarID;
                tbMake.Text = value.Make;
                tbModel.Text = value.Model;
                numYear.Value = value.Year;
                numCity.Value = value.MpgCity;
                numHighway.Value = value.MpgHighway;
            }
        }

        // -----------------------------------------------------------------------------
        // Triggers
        // -----------------------------------------------------------------------------
        // Button to update and modify current object
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            // Call modify object method
            ModifyObjectEvent();
        }
        // Button to go to the beginning
        private void btnBeginning_Click(object sender, EventArgs e)
        {
            // Reset the index of the row back to zero
            row = 0;
            // Update all of the data fields to the beginning of the data
            tbID.Text= theData.Rows[0]["vehicleId"].ToString();
            tbMake.Text = theData.Rows[0]["make"].ToString();
            tbModel.Text = theData.Rows[0]["model"].ToString();
            numYear.Value = Convert.ToDecimal(theData.Rows[0]["year"]);
            numCity.Value = Convert.ToDecimal(theData.Rows[0]["mpgCity"]);
            numHighway.Value = Convert.ToDecimal(theData.Rows[0]["mpgHighway"]);
            // Update the current record plus one for the index
            lblCurrentRecord.Text = (row + 1).ToString();
        }
        // Button to go to the end
        private void btnEnd_Click(object sender, EventArgs e)
        {
            // Put the row to the last of the length minus 1 for the index
            row = (theData.Select().Length) - 1;
            // Update all of the data fields
            tbID.Text = theData.Rows[row]["vehicleId"].ToString();
            tbMake.Text = theData.Rows[row]["make"].ToString();
            tbModel.Text = theData.Rows[row]["model"].ToString();
            numYear.Value = Convert.ToDecimal(theData.Rows[row]["year"]);
            numCity.Value = Convert.ToDecimal(theData.Rows[row]["mpgCity"]);
            numHighway.Value = Convert.ToDecimal(theData.Rows[row]["mpgHighway"]);
            // Update the current record
            lblCurrentRecord.Text = (row + 1).ToString();
        }
        // Button to go forward 1 index
        private void btnForward_Click(object sender, EventArgs e)
        {
            // Make sure we haven't exceeded the last record
            if (row + 1 < theData.Select().Length)
            {
                // Add one to the row and update the data fields
                row++;

                // Update the data showing on the form
                tbID.Text = theData.Rows[row]["vehicleId"].ToString();
                tbMake.Text = theData.Rows[row]["make"].ToString();
                tbModel.Text = theData.Rows[row]["model"].ToString();
                numYear.Value = Convert.ToDecimal(theData.Rows[row]["year"]);
                numCity.Value = Convert.ToDecimal(theData.Rows[row]["mpgCity"]);
                numHighway.Value = Convert.ToDecimal(theData.Rows[row]["mpgHighway"]);
                // Update the current record
                lblCurrentRecord.Text = (row + 1).ToString();
            }
        }
        // Button to go back 1 index
        private void btnBack_Click(object sender, EventArgs e)
        {
            // As long as the index is not zero it will minus one
            if (row != 0)
            {
                // Subtract from the row to go back one level
                row--;

                // Update the data showing on the form
                tbID.Text = theData.Rows[row]["vehicleId"].ToString();
                tbMake.Text = theData.Rows[row]["make"].ToString();
                tbModel.Text = theData.Rows[row]["model"].ToString();
                numYear.Value = Convert.ToDecimal(theData.Rows[row]["year"]);
                numCity.Value = Convert.ToDecimal(theData.Rows[row]["mpgCity"]);
                numHighway.Value = Convert.ToDecimal(theData.Rows[row]["mpgHighway"]);
                // Update the current record
                lblCurrentRecord.Text = (row + 1).ToString();
            }
        }
        // Event trigger when the main form loads
        private void mainForm_Load(object sender, EventArgs e)
        {
            // Make the default display icons small
            menuSmall.Checked = true;
            menuSmall.Enabled = false;

            // Sub to the events
            SelectionChanged += SelectionChangedHandler;
            ObjectAdded += ObjectAddedHandler;
            ModifyObject += HandleModifyObject;

            // Set the variables
            string userName = "dbsAdmin";
            string passWord = "password";
            string dataBase = "exampleDatabase";

            // Build the connection string with a try catch
            // Using 3 arguments for future use
            string connectionString = BuildConnectionString(dataBase, userName, passWord);

            // Connect to the database with arguments for future use
            Connect(connectionString, dataBase);
            
            // Call the retrieve data method to start filling out the data fields
            RetrieveData();

            // Change the status to make sure we connected
            connectStatus.Text = "Connected to: " + dataBase;

            // Add all of the data to list view
            FillData();

            Data = new Cars();
        }
        // Menu button to make large icons
        private void menuLarge_Click(object sender, EventArgs e)
        {
            // Make sure that the checks are in the right spot
            menuSmall.Checked = false;
            menuLarge.Enabled = false;

            // Change the icons
            if (menuLarge.Checked == true)
            {
                // Change to large icons
                carListView.View = View.LargeIcon;
                // Make sure that the small menu check is false
                menuSmall.Enabled = true;
            }
        }
        // Menu button to make small icons
        private void menuSmall_Click(object sender, EventArgs e)
        {
            // Make sure that the checks are in the right spot
            menuLarge.Checked = false;
            menuSmall.Enabled = false;

            // Change the icons
            if (menuSmall.Checked == true)
            {
                // Change to small icons
                carListView.View = View.SmallIcon;
                // Make sure that the large menu check is false
                menuLarge.Enabled = true;
            }
        }
        // Menu button to exit the application
        private void menuExit_Click(object sender, EventArgs e)
        {
            // Close out of the application
            Application.Exit();
        }

        // -----------------------------------------------------------------------------
        // Object Handlers
        // -----------------------------------------------------------------------------
        // Added object handler
        public void ObjectAddedHandler(object sender, EventArgs e)
        {
            // Set the data table as the sender
            DataTable dt = sender as DataTable;
            // Set the cars to the data
            Cars c = Data;
            // Set new list view item
            ListViewItem lvi = new ListViewItem();

            // Set the text as the list view item
            lvi.Text = c.ToString();
            // Set the image index
            lvi.ImageIndex = c.ImageIndex;
            // Set the new tag as the current car
            lvi.Tag = c;

            // Add the new item as the current object
            carListView.Items.Add(lvi);
        }

        private void SelectionChangedHandler(object sender, EventArgs e)
        {
            // Set the data to the selected object
            Data = SelectedObject;
        }

        // -----------------------------------------------------------------------------
        // Change Events
        // -----------------------------------------------------------------------------
        // As the selected item changes change the input fields
        private void carListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Invoke the selection changed handler
            SelectionChanged?.Invoke(this, new EventArgs());
        }
        
        // Modify the current selected object
        public void HandleModifyObject(object sender, ModifyObjectEventArgs e)
        {
            // Get all values from input fields
            Cars c = e.ObjectToModify1.Tag as Cars;
            c.CarID = tbID.Text;
            c.Make = tbMake.Text;
            c.Model = tbModel.Text;
            c.Year = numYear.Value;
            c.MpgCity = numCity.Value;
            c.MpgHighway = numHighway.Value;
            c.ImageIndex = 0;
            // Set all of the values to the new object
            e.ObjectToModify1.Text = c.ToString();
            // Set the image as well
            e.ObjectToModify1.ImageIndex = c.ImageIndex;
        }

        // -----------------------------------------------------------------------------
        // Methods
        // -----------------------------------------------------------------------------
        // Build the connection string with arguments for future use
        private string BuildConnectionString(string database, string uid, string pword)
        {
            // Set a blank server IP at first
            string serverIP = "";

            try
            {
                // Open the text file using a stream reader
                using (StreamReader sr = new StreamReader("C:\\VFW\\connect.txt"))
                {
                    // Read the server IP data
                    serverIP = sr.ReadToEnd();
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }

            // Return the entire string
            return "server=" + serverIP + ";uid=" + uid + ";pwd=" + pword + ";database=" + database + ";port=3306";
        }

        // Method to connect to the database
        // with a try catch to handle any errors
        private void Connect(string myConnectionString, string database)
        {
            try
            {
                // Try to open the connection
                // Catch it if we cannot connect
                conn.ConnectionString = myConnectionString;
                conn.Open();
            }

            // Using the MySql specific exceptions
            catch (MySqlException e)
            {
                // Message string variable
                string msg = "";

                // Check what exception was received
                switch (e.Number)
                {
                    case 0:
                        msg = e.ToString();
                        break;
                    case 1042:
                        msg = "Can't resolve hos address.\n" + myConnectionString;
                        break;
                    case 1045:
                        msg = "Invalid username/password";
                        break;
                    default:
                        // Generic message if the others don't cover it
                        msg = e.ToString() + "\n" + myConnectionString;
                        break;
                }

                // Show the error on why we cannot connect
                MessageBox.Show(msg);
            }
        }

        // Method to retrieve the data from MySQL
        private void RetrieveData()
        {
            // Create the DataAdapter
            MySqlDataAdapter adr = new MySqlDataAdapter(dwnSQL, conn);

            // Set the type for the SELECT command to text
            adr.SelectCommand.CommandType = CommandType.Text;

            // The fill method adds rows to match the data source
            // Fill the DataTable with the record-set returned by the DataAdapter
            adr.Fill(theData);

            // Getting a count of the number of rows within the DataTable
            int numberOfRecords = theData.Select().Length;

            // Set the current record
            lblCurrentRecord.Text = (row + 1).ToString();

            // Set the total number of records
            lblTotalRecord.Text = numberOfRecords.ToString();

        }

        // Fill out the data in the list-view
        private void FillData()
        {
            // Look at how many rows there are
            for (int i = 0; i < theData.Rows.Count; i++)
            {
                // Set the current data row
                DataRow dr = theData.Rows[i];

                // Fill out all data fields with the data in database
                tbID.Text = theData.Rows[i]["vehicleId"].ToString();
                tbMake.Text = theData.Rows[i]["make"].ToString();
                tbModel.Text = theData.Rows[i]["model"].ToString();
                numYear.Value = Convert.ToDecimal(theData.Rows[i]["year"]);
                numCity.Value = Convert.ToDecimal(theData.Rows[i]["mpgCity"]);
                numHighway.Value = Convert.ToDecimal(theData.Rows[i]["mpgHighway"]);

                // Set the object to the current data
                Cars cars = Data;

                // Make sure that the object is not null
                if (ObjectAdded != null)
                {
                    // Add the object to the list using the object handler
                    ObjectAdded(this, new EventArgs());
                }
            }
        }

        // Modify the current selected item
        public void ModifyObjectEvent()
        {
            // Ask the user if they really want to change the data
            DialogResult dialogResult = MessageBox.Show("You are about to overwrite" +
                " the current selected item.", "Are You Sure?", MessageBoxButtons.YesNo);

            // Handle the yes
            if (dialogResult == DialogResult.Yes)
            {
                // Make sure the user has selected something
                if (ModifyObject != null && carListView.SelectedItems.Count > 0)
                {
                    // Modify the current selected item
                    ModifyObject(this, new ModifyObjectEventArgs(carListView.SelectedItems[0]));

                    // Call update database method
                    UpdateDataBase();

                    // Clear everything
                    Clear();

                    // Refresh the data with the new updated data
                    RetrieveData();

                    // Fill the data back in the list view
                    FillData();

                    // Clear the input fields
                    Data = new Cars();

                    // Tell the user that the save was successful
                    MessageBox.Show("Updated!", "Success!", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
                else
                {
                    // Ask the user to select an object to modify
                    MessageBox.Show("Please highlight something to update.", "Oops",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        // Method to update database
        public void UpdateDataBase()
        {
            // Variables to hold data
            int i = Convert.ToInt32(tbID.Text);
            int yr = Convert.ToInt32(numYear.Value);

            // Update query to run in MySql
            string query = "UPDATE exampledatabase.vehicle SET make=@Make ,model=@Model, year=@Year ," +
                " mpgCity=@MpgCity, mpgHighway=@MpgHighway WHERE vehicleId=@VehicleId";

            // Create new sql command
            MySqlCommand cmd = new MySqlCommand();
            // Add the command text
            cmd.CommandText = query;
            // Pass all of the parameters to the database
            cmd.Parameters.AddWithValue("@Make", tbMake.Text);
            cmd.Parameters.AddWithValue("@Model", tbModel.Text);
            cmd.Parameters.AddWithValue("@Year", yr);
            cmd.Parameters.AddWithValue("@MpgCity", numCity.Value);
            cmd.Parameters.AddWithValue("@MpgHighway", numHighway.Value);
            cmd.Parameters.AddWithValue("@VehicleId", i);
            // Link into open connection
            cmd.Connection = conn;
            // Execute the new query
            cmd.ExecuteNonQuery();
        }

        // Method to clear everything
        private void Clear()
        {
            // Clear the data table
            theData.Clear();

            // Clear the list view
            carListView.Clear();
        }
    }
}
