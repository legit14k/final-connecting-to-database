﻿namespace JasonGrimberg_Final
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.gbDatabaseData = new System.Windows.Forms.GroupBox();
            this.numCity = new System.Windows.Forms.NumericUpDown();
            this.numHighway = new System.Windows.Forms.NumericUpDown();
            this.numYear = new System.Windows.Forms.NumericUpDown();
            this.tbModel = new System.Windows.Forms.TextBox();
            this.tbMake = new System.Windows.Forms.TextBox();
            this.lblmpgCity = new System.Windows.Forms.Label();
            this.lblmpgHighway = new System.Windows.Forms.Label();
            this.lblYear = new System.Windows.Forms.Label();
            this.lblModel = new System.Windows.Forms.Label();
            this.lblMake = new System.Windows.Forms.Label();
            this.btnBeginning = new System.Windows.Forms.Button();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnEnd = new System.Windows.Forms.Button();
            this.btnForward = new System.Windows.Forms.Button();
            this.gbListView = new System.Windows.Forms.GroupBox();
            this.carListView = new System.Windows.Forms.ListView();
            this.ilLargeImage = new System.Windows.Forms.ImageList(this.components);
            this.ilSmallImage = new System.Windows.Forms.ImageList(this.components);
            this.lblTotalRecord = new System.Windows.Forms.Label();
            this.lblCurrentRecord = new System.Windows.Forms.Label();
            this.lblRecord = new System.Windows.Forms.Label();
            this.lblOf = new System.Windows.Forms.Label();
            this.menuMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuExit = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuLarge = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSmall = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.connectStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbID = new System.Windows.Forms.TextBox();
            this.gbDatabaseData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHighway)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numYear)).BeginInit();
            this.gbListView.SuspendLayout();
            this.menuMain.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbDatabaseData
            // 
            this.gbDatabaseData.Controls.Add(this.label1);
            this.gbDatabaseData.Controls.Add(this.btnUpdate);
            this.gbDatabaseData.Controls.Add(this.numCity);
            this.gbDatabaseData.Controls.Add(this.numHighway);
            this.gbDatabaseData.Controls.Add(this.numYear);
            this.gbDatabaseData.Controls.Add(this.tbModel);
            this.gbDatabaseData.Controls.Add(this.tbID);
            this.gbDatabaseData.Controls.Add(this.tbMake);
            this.gbDatabaseData.Controls.Add(this.lblmpgCity);
            this.gbDatabaseData.Controls.Add(this.lblmpgHighway);
            this.gbDatabaseData.Controls.Add(this.lblYear);
            this.gbDatabaseData.Controls.Add(this.lblModel);
            this.gbDatabaseData.Controls.Add(this.lblMake);
            this.gbDatabaseData.Controls.Add(this.btnBeginning);
            this.gbDatabaseData.Controls.Add(this.btnBack);
            this.gbDatabaseData.Controls.Add(this.btnEnd);
            this.gbDatabaseData.Controls.Add(this.btnForward);
            this.gbDatabaseData.Location = new System.Drawing.Point(12, 27);
            this.gbDatabaseData.Name = "gbDatabaseData";
            this.gbDatabaseData.Size = new System.Drawing.Size(501, 268);
            this.gbDatabaseData.TabIndex = 2;
            this.gbDatabaseData.TabStop = false;
            this.gbDatabaseData.Text = "Vehicle Info";
            // 
            // numCity
            // 
            this.numCity.DecimalPlaces = 4;
            this.numCity.Location = new System.Drawing.Point(153, 182);
            this.numCity.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numCity.Name = "numCity";
            this.numCity.Size = new System.Drawing.Size(120, 20);
            this.numCity.TabIndex = 5;
            // 
            // numHighway
            // 
            this.numHighway.DecimalPlaces = 4;
            this.numHighway.Location = new System.Drawing.Point(153, 149);
            this.numHighway.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numHighway.Name = "numHighway";
            this.numHighway.Size = new System.Drawing.Size(120, 20);
            this.numHighway.TabIndex = 4;
            // 
            // numYear
            // 
            this.numYear.Location = new System.Drawing.Point(153, 116);
            this.numYear.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numYear.Name = "numYear";
            this.numYear.Size = new System.Drawing.Size(120, 20);
            this.numYear.TabIndex = 3;
            // 
            // tbModel
            // 
            this.tbModel.Location = new System.Drawing.Point(153, 83);
            this.tbModel.Name = "tbModel";
            this.tbModel.Size = new System.Drawing.Size(226, 20);
            this.tbModel.TabIndex = 2;
            // 
            // tbMake
            // 
            this.tbMake.Location = new System.Drawing.Point(153, 49);
            this.tbMake.Name = "tbMake";
            this.tbMake.Size = new System.Drawing.Size(226, 20);
            this.tbMake.TabIndex = 1;
            // 
            // lblmpgCity
            // 
            this.lblmpgCity.AutoSize = true;
            this.lblmpgCity.Location = new System.Drawing.Point(59, 184);
            this.lblmpgCity.Name = "lblmpgCity";
            this.lblmpgCity.Size = new System.Drawing.Size(57, 13);
            this.lblmpgCity.TabIndex = 8;
            this.lblmpgCity.Text = "MPG City: ";
            // 
            // lblmpgHighway
            // 
            this.lblmpgHighway.AutoSize = true;
            this.lblmpgHighway.Location = new System.Drawing.Point(35, 151);
            this.lblmpgHighway.Name = "lblmpgHighway";
            this.lblmpgHighway.Size = new System.Drawing.Size(81, 13);
            this.lblmpgHighway.TabIndex = 7;
            this.lblmpgHighway.Text = "MPG Highway: ";
            // 
            // lblYear
            // 
            this.lblYear.AutoSize = true;
            this.lblYear.Location = new System.Drawing.Point(81, 123);
            this.lblYear.Name = "lblYear";
            this.lblYear.Size = new System.Drawing.Size(35, 13);
            this.lblYear.TabIndex = 6;
            this.lblYear.Text = "Year: ";
            // 
            // lblModel
            // 
            this.lblModel.AutoSize = true;
            this.lblModel.Location = new System.Drawing.Point(74, 86);
            this.lblModel.Name = "lblModel";
            this.lblModel.Size = new System.Drawing.Size(42, 13);
            this.lblModel.TabIndex = 5;
            this.lblModel.Text = "Model: ";
            // 
            // lblMake
            // 
            this.lblMake.AutoSize = true;
            this.lblMake.Location = new System.Drawing.Point(76, 52);
            this.lblMake.Name = "lblMake";
            this.lblMake.Size = new System.Drawing.Size(40, 13);
            this.lblMake.TabIndex = 4;
            this.lblMake.Text = "Make: ";
            // 
            // btnBeginning
            // 
            this.btnBeginning.Location = new System.Drawing.Point(20, 227);
            this.btnBeginning.Name = "btnBeginning";
            this.btnBeginning.Size = new System.Drawing.Size(110, 23);
            this.btnBeginning.TabIndex = 6;
            this.btnBeginning.Text = "<< Beginning";
            this.btnBeginning.UseVisualStyleBackColor = true;
            this.btnBeginning.Click += new System.EventHandler(this.btnBeginning_Click);
            // 
            // btnBack
            // 
            this.btnBack.Location = new System.Drawing.Point(136, 227);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(110, 23);
            this.btnBack.TabIndex = 7;
            this.btnBack.Text = "< Back";
            this.btnBack.UseVisualStyleBackColor = true;
            this.btnBack.Click += new System.EventHandler(this.btnBack_Click);
            // 
            // btnEnd
            // 
            this.btnEnd.Location = new System.Drawing.Point(368, 227);
            this.btnEnd.Name = "btnEnd";
            this.btnEnd.Size = new System.Drawing.Size(110, 23);
            this.btnEnd.TabIndex = 9;
            this.btnEnd.Text = "End >>";
            this.btnEnd.UseVisualStyleBackColor = true;
            this.btnEnd.Click += new System.EventHandler(this.btnEnd_Click);
            // 
            // btnForward
            // 
            this.btnForward.Location = new System.Drawing.Point(252, 227);
            this.btnForward.Name = "btnForward";
            this.btnForward.Size = new System.Drawing.Size(110, 23);
            this.btnForward.TabIndex = 8;
            this.btnForward.Text = "Forward >";
            this.btnForward.UseVisualStyleBackColor = true;
            this.btnForward.Click += new System.EventHandler(this.btnForward_Click);
            // 
            // gbListView
            // 
            this.gbListView.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbListView.Controls.Add(this.carListView);
            this.gbListView.Location = new System.Drawing.Point(522, 27);
            this.gbListView.Name = "gbListView";
            this.gbListView.Size = new System.Drawing.Size(390, 325);
            this.gbListView.TabIndex = 4;
            this.gbListView.TabStop = false;
            this.gbListView.Text = "List Of Cars";
            // 
            // carListView
            // 
            this.carListView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.carListView.LargeImageList = this.ilLargeImage;
            this.carListView.Location = new System.Drawing.Point(3, 16);
            this.carListView.MultiSelect = false;
            this.carListView.Name = "carListView";
            this.carListView.Size = new System.Drawing.Size(384, 306);
            this.carListView.SmallImageList = this.ilSmallImage;
            this.carListView.TabIndex = 2;
            this.carListView.UseCompatibleStateImageBehavior = false;
            this.carListView.View = System.Windows.Forms.View.SmallIcon;
            this.carListView.SelectedIndexChanged += new System.EventHandler(this.carListView_SelectedIndexChanged);
            // 
            // ilLargeImage
            // 
            this.ilLargeImage.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilLargeImage.ImageStream")));
            this.ilLargeImage.TransparentColor = System.Drawing.Color.Transparent;
            this.ilLargeImage.Images.SetKeyName(0, "Large.jpg");
            // 
            // ilSmallImage
            // 
            this.ilSmallImage.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilSmallImage.ImageStream")));
            this.ilSmallImage.TransparentColor = System.Drawing.Color.Transparent;
            this.ilSmallImage.Images.SetKeyName(0, "Small.jpg");
            // 
            // lblTotalRecord
            // 
            this.lblTotalRecord.AutoSize = true;
            this.lblTotalRecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalRecord.Location = new System.Drawing.Point(489, 314);
            this.lblTotalRecord.Name = "lblTotalRecord";
            this.lblTotalRecord.Size = new System.Drawing.Size(24, 26);
            this.lblTotalRecord.TabIndex = 21;
            this.lblTotalRecord.Text = "0";
            // 
            // lblCurrentRecord
            // 
            this.lblCurrentRecord.AutoSize = true;
            this.lblCurrentRecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCurrentRecord.Location = new System.Drawing.Point(423, 314);
            this.lblCurrentRecord.Name = "lblCurrentRecord";
            this.lblCurrentRecord.Size = new System.Drawing.Size(24, 26);
            this.lblCurrentRecord.TabIndex = 20;
            this.lblCurrentRecord.Text = "0";
            // 
            // lblRecord
            // 
            this.lblRecord.AutoSize = true;
            this.lblRecord.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRecord.Location = new System.Drawing.Point(323, 314);
            this.lblRecord.Name = "lblRecord";
            this.lblRecord.Size = new System.Drawing.Size(94, 26);
            this.lblRecord.TabIndex = 18;
            this.lblRecord.Text = "Record: ";
            // 
            // lblOf
            // 
            this.lblOf.AutoSize = true;
            this.lblOf.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOf.Location = new System.Drawing.Point(453, 314);
            this.lblOf.Name = "lblOf";
            this.lblOf.Size = new System.Drawing.Size(30, 26);
            this.lblOf.TabIndex = 19;
            this.lblOf.Text = "of";
            // 
            // menuMain
            // 
            this.menuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem});
            this.menuMain.Location = new System.Drawing.Point(0, 0);
            this.menuMain.Name = "menuMain";
            this.menuMain.Size = new System.Drawing.Size(925, 24);
            this.menuMain.TabIndex = 22;
            this.menuMain.Text = "Menu";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuExit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // menuExit
            // 
            this.menuExit.Name = "menuExit";
            this.menuExit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.menuExit.Size = new System.Drawing.Size(135, 22);
            this.menuExit.Text = "E&xit";
            this.menuExit.Click += new System.EventHandler(this.menuExit_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuLarge,
            this.menuSmall});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.viewToolStripMenuItem.Text = "&View";
            // 
            // menuLarge
            // 
            this.menuLarge.CheckOnClick = true;
            this.menuLarge.Name = "menuLarge";
            this.menuLarge.Size = new System.Drawing.Size(103, 22);
            this.menuLarge.Text = "&Large";
            this.menuLarge.Click += new System.EventHandler(this.menuLarge_Click);
            // 
            // menuSmall
            // 
            this.menuSmall.CheckOnClick = true;
            this.menuSmall.Name = "menuSmall";
            this.menuSmall.Size = new System.Drawing.Size(103, 22);
            this.menuSmall.Text = "S&mall";
            this.menuSmall.Click += new System.EventHandler(this.menuSmall_Click);
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectStatus});
            this.statusStrip.Location = new System.Drawing.Point(0, 363);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(925, 22);
            this.statusStrip.TabIndex = 23;
            this.statusStrip.Text = "statusStrip1";
            // 
            // connectStatus
            // 
            this.connectStatus.Name = "connectStatus";
            this.connectStatus.Size = new System.Drawing.Size(85, 17);
            this.connectStatus.Text = "Connected to: ";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(368, 182);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(110, 23);
            this.btnUpdate.TabIndex = 10;
            this.btnUpdate.Text = "Update Data";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(92, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "ID: ";
            // 
            // tbID
            // 
            this.tbID.Location = new System.Drawing.Point(153, 17);
            this.tbID.Name = "tbID";
            this.tbID.ReadOnly = true;
            this.tbID.Size = new System.Drawing.Size(226, 20);
            this.tbID.TabIndex = 0;
            // 
            // mainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(925, 385);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.menuMain);
            this.Controls.Add(this.lblTotalRecord);
            this.Controls.Add(this.lblCurrentRecord);
            this.Controls.Add(this.lblRecord);
            this.Controls.Add(this.lblOf);
            this.Controls.Add(this.gbListView);
            this.Controls.Add(this.gbDatabaseData);
            this.Name = "mainForm";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "New Car";
            this.Load += new System.EventHandler(this.mainForm_Load);
            this.gbDatabaseData.ResumeLayout(false);
            this.gbDatabaseData.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numCity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numHighway)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numYear)).EndInit();
            this.gbListView.ResumeLayout(false);
            this.menuMain.ResumeLayout(false);
            this.menuMain.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDatabaseData;
        private System.Windows.Forms.NumericUpDown numCity;
        private System.Windows.Forms.NumericUpDown numHighway;
        private System.Windows.Forms.NumericUpDown numYear;
        private System.Windows.Forms.TextBox tbModel;
        private System.Windows.Forms.TextBox tbMake;
        private System.Windows.Forms.Label lblmpgCity;
        private System.Windows.Forms.Label lblmpgHighway;
        private System.Windows.Forms.Label lblYear;
        private System.Windows.Forms.Label lblModel;
        private System.Windows.Forms.Label lblMake;
        private System.Windows.Forms.Button btnBeginning;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnEnd;
        private System.Windows.Forms.Button btnForward;
        private System.Windows.Forms.GroupBox gbListView;
        private System.Windows.Forms.ListView carListView;
        private System.Windows.Forms.Label lblTotalRecord;
        private System.Windows.Forms.Label lblCurrentRecord;
        private System.Windows.Forms.Label lblRecord;
        private System.Windows.Forms.Label lblOf;
        private System.Windows.Forms.MenuStrip menuMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuExit;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem menuLarge;
        private System.Windows.Forms.ToolStripMenuItem menuSmall;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel connectStatus;
        private System.Windows.Forms.ImageList ilLargeImage;
        private System.Windows.Forms.ImageList ilSmallImage;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbID;
    }
}

